'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var EventSchema = new Schema({
  title: String,
  start: String,
  end: String
});

module.exports = mongoose.model('Event', EventSchema);