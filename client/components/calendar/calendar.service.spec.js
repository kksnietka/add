'use strict';

describe('Service: Calendar', function () {

  // load the service's module
  beforeEach(module('nodeTubeApp'));

  // instantiate service
  var calendar;
  beforeEach(inject(function (_calendar_) {
    calendar = _calendar_;
  }));

  it('should do something', function () {
    expect(!!calendar).toBe(true);
  });

});
