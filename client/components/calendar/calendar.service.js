'use strict';

angular.module('nodeTubeApp')
    .service('Calendar', ['$http', '$q',
        function($http, $q) {
            // AngularJS will instantiate a singleton by calling "new" on this function

            var calendar = {};


            var eventDrop = function() {
                console.log(arguments);
                console.log(calendar.eventSources);

            }


            calendar.uiConfig = {
                calendar: {
                    height: 450,
                    editable: true,
                    header: {
                        left: 'month basicWeek basicDay agendaWeek agendaDay',
                        center: 'title',
                        right: 'today prev,next'
                    },

                    eventDrop: eventDrop

                }
            };





            return calendar;




        }
    ]);